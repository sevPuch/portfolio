let scene, camera, mesh, loader, renderer;
let player = new THREE.Object3D();
let walkSpeed = 0.4;
let url = "poop/poop.js";

let moveForward, moveLeft, moveRight, moveBack;
document.addEventListener("keydown", keyDownHandle, false);
document.addEventListener("keyup", keyUpHandle, false);

let SCREEN_WIDTH = window.innerWidth*0.8;
let SCREEN_HEIGHT = window.innerHeight*0.8;

let container = document.getElementById("container");

// RESIZE LISTENER NOT WORKING
window.addEventListener( 'resize', onWindowResize, false );

//////////////////////////////////////////
/////////// Initialize //////////////////
///////////////////////////////
// RENDERER
renderer = new THREE.WebGLRenderer({antialias: true});
renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
renderer.shadowMapEnabled = true;
container.appendChild(renderer.domElement);
// CAMERA
player.add(camera);
camera = new THREE.PerspectiveCamera(70, SCREEN_WIDTH/SCREEN_HEIGHT, 0.5, 10000);
camera.position.set(0, 5, 10);
camera.rotation.x = -10 * Math.PI/180;

// SCENE
scene = new THREE.Scene();
scene.background = new THREE.Color(0xffffff);


//////////////////////////// WORLD ///////

////////////////////////
/////////// LUMIERES
let pointLight = new THREE.PointLight(0x555555, 3, 100);
pointLight.position.set(-2,8,5);
pointLight.castShadow = true;
scene.add(pointLight);

let pointLight2 = new THREE.PointLight(0xFFFF66, 1, 10);
pointLight2.position.set(2.5,8,-13.5);
pointLight2.castShadow = true;
scene.add(pointLight2);
let pointLight4 = new THREE.PointLight(0xFFFF66, 1, 10);
pointLight4.position.set(4.7,8,-13);
pointLight4.castShadow = true;
scene.add(pointLight4);

let pointLight3 = new THREE.PointLight(0xFFaa66, 2, 5);
pointLight3.position.set(-2,4,-14);
pointLight3.castShadow = true;
scene.add(pointLight3);


// Cube
let geometry = new THREE.BoxGeometry(2,2.4,5);
let material = new THREE.MeshBasicMaterial({color: 0x209910, roughness: 0.5, metalness: 1.0});
let cube = new THREE.Mesh(geometry, material);
cube.position.set(0,20,-10);
scene.add(cube);
// Ground
let groundGeom = new THREE.PlaneBufferGeometry(1000, 1000);
let groundMat = new THREE.MeshStandardMaterial({
    color: 0xfefefe,
    // shininess: 1,
    metalness:0,
    roughness:1
});
let ground = new THREE.Mesh(groundGeom, groundMat);
ground.position.set(0, 0, 0);
ground.rotation.x = - Math.PI/2;
ground.receiveShadow = true;
scene.add(ground);

// Fog
scene.fog = new THREE.Fog(0xffffff, 5, 50);


////////////////////////////////////////////// load model from JSON file
// LOADER
loader = new THREE.JSONLoader();
loader.load(
    "3D/prometheus.js",
    function(geometry){
        let poopText = new THREE.ImageUtils.loadTexture("3D/prometheusNORMAL.png")
        let poopMat = new THREE.MeshStandardMaterial({
            // map: poopText,
            color: 0xfafafa,
            roughness: 0.3,
            metalness: 0,
            // map.wrapS: THREE.RepeatWrapping,
            // neheTexture.wrapT: THREE.RepeatWrapping, 
            // side: THREE.DoubleSide
        })
        poopMat.normalMap = poopText;

        mesh = new THREE.Mesh(geometry, poopMat);        
        mesh.position.set(-10,0,0);
        mesh.rotation.y = -153*Math.PI/180;
        // mesh.scale.set(0.1,0.1,0.1);
        mesh.castShadow = true;
        scene.add(mesh);
    }
)
loader.load(
    "3D/rupert.js",
    function(geometry){
        // let poopText = new THREE.ImageUtils.loadTexture("poop/austin.jpg")
        let poopMat = new THREE.MeshStandardMaterial({
            // map: poopText,
            color: 0xfafafa,
            roughness: 0.6,
            metalness: 0,
            // map.wrapS: THREE.RepeatWrapping,
            // neheTexture.wrapT: THREE.RepeatWrapping, 
            side: THREE.DoubleSide
        })

        let mesh2 = new THREE.Mesh(geometry, poopMat);        
        mesh2.position.set(7,0,0);
        mesh2.rotation.y = -130*Math.PI/180;
        // mesh.scale.set(0.1,0.1,0.1);
        mesh2.castShadow = true;
        scene.add(mesh2);
    }
)
loader.load(
    "3D/monstre.js",
    function(geometry){
        let poopText = new THREE.ImageUtils.loadTexture("3D/monstreNORMAL.png");
        let poopMat = new THREE.MeshStandardMaterial({
            // map: poopText,
            color: 0xfafafa,
            roughness: 0.4,
            metalness: 0.1,
            // map.wrapS: THREE.RepeatWrapping,
            // neheTexture.wrapT: THREE.RepeatWrapping, 
            side: THREE.DoubleSide
        })
        poopMat.normalMap = poopText;
        
        let mesh2 = new THREE.Mesh(geometry, poopMat);        
        mesh2.position.set(-4,0,-6);
        mesh2.rotation.y = -70*Math.PI/180;
        mesh2.scale.set(1.5,1.5,1.5);
        mesh2.castShadow = true;
        scene.add(mesh2);
    }
)
loader.load(
    "3D/lamp.js",
    function(geometry){
        // let poopText = new THREE.ImageUtils.loadTexture("poop/austin.jpg")
        let poopMat = new THREE.MeshStandardMaterial({
            // map: poopText,
            color: 0xfafafa,
            roughness: 0.6,
            metalness: 0,
            // map.wrapS: THREE.RepeatWrapping,
            // neheTexture.wrapT: THREE.RepeatWrapping, 
            side: THREE.DoubleSide
        })

        let mesh2 = new THREE.Mesh(geometry, poopMat);        
        mesh2.position.set(5,6,-15);
        mesh2.rotation.y = -130*Math.PI/180;
        mesh2.scale.set(0.5,0.5,0.5);
        mesh2.castShadow = true;
        scene.add(mesh2);
    }
)
loader.load(
    "3D/chair.js",
    function(geometry){
        // let poopText = new THREE.ImageUtils.loadTexture("poop/austin.jpg")
        let poopMat = new THREE.MeshStandardMaterial({
            // map: poopText,
            color: 0xfafafa,
            roughness: 0.6,
            metalness: 0,
            // map.wrapS: THREE.RepeatWrapping,
            // neheTexture.wrapT: THREE.RepeatWrapping, 
            side: THREE.DoubleSide
        })

        let mesh2 = new THREE.Mesh(geometry, poopMat);        
        mesh2.position.set(6,0,-12);
        mesh2.rotation.y = -130*Math.PI/180;
        mesh2.scale.set(1.2,1.2,1.2);
        mesh2.castShadow = true;
        scene.add(mesh2);
    }
)
loader.load(
    "3D/beer.js",
    function(geometry){
        // let poopText = new THREE.ImageUtils.loadTexture("poop/austin.jpg")
        let poopMat = new THREE.MeshStandardMaterial({
            // map: poopText,
            color: 0xfafafa,
            roughness: 0.6,
            metalness: 0,
            // map.wrapS: THREE.RepeatWrapping,
            // neheTexture.wrapT: THREE.RepeatWrapping, 
            side: THREE.DoubleSide
        })

        let mesh2 = new THREE.Mesh(geometry, poopMat);        
        mesh2.position.set(8,0,-9);
        mesh2.rotation.y = -130*Math.PI/180;
        mesh2.scale.set(0.4,0.4,0.4);
        mesh2.castShadow = true;
        scene.add(mesh2);
    }
)
loader.load(
    "3D/cierge.js",
    function(geometry){
        // let poopText = new THREE.ImageUtils.loadTexture("poop/austin.jpg")
        let poopMat = new THREE.MeshStandardMaterial({
            // map: poopText,
            color: 0xfafafa,
            roughness: 0.6,
            metalness: 0,
            // map.wrapS: THREE.RepeatWrapping,
            // neheTexture.wrapT: THREE.RepeatWrapping, 
            side: THREE.DoubleSide
        })

        let mesh2 = new THREE.Mesh(geometry, poopMat);        
        mesh2.position.set(-2,0,-14);
        mesh2.rotation.y = -130*Math.PI/180;
        mesh2.scale.set(0.7,0.7,0.7);
        mesh2.castShadow = true;
        scene.add(mesh2);
    }
)
loader.load(
    "3D/fork.js",
    function(geometry){
        // let poopText = new THREE.ImageUtils.loadTexture("poop/austin.jpg")
        let poopMat = new THREE.MeshStandardMaterial({
            // map: poopText,
            color: 0xfafafa,
            roughness: 0.6,
            metalness: 0,
            // map.wrapS: THREE.RepeatWrapping,
            // neheTexture.wrapT: THREE.RepeatWrapping, 
            side: THREE.DoubleSide
        })

        let mesh2 = new THREE.Mesh(geometry, poopMat);        
        mesh2.position.set(5.3,2.6,-12);
        mesh2.rotation.y = 15*Math.PI/180;
        mesh2.scale.set(0.2,0.2,0.2);
        mesh2.castShadow = true;
        scene.add(mesh2);
    }
)
loader.load(
    "poop/poop.js",
    function(geometry){
        let poopText2 = new THREE.ImageUtils.loadTexture("poop/austin.jpg")
        let poopMat2 = new THREE.MeshStandardMaterial({
            // map: poopText,
            color: 0x904020,
            roughness: 0.6,
            metalness: 0
            // map.wrapS: THREE.RepeatWrapping,
            // neheTexture.wrapT: THREE.RepeatWrapping, 
            // side: THREE.DoubleSide
        })

        mesh2 = new THREE.Mesh(geometry, poopMat2);        
        mesh2.position.set(1,0,0);

        scene.add(mesh2);
    }
)


renderer.render(scene, camera);


/////////////////////
////// FUNCTIONS
function keyDownHandle(event){
    if(event.keyCode == 37){
        moveLeft = true;
    }
    if(event.keyCode == 38){
        moveForward = true;
    }
    if(event.keyCode == 39){
        moveRight = true;
    }
    if(event.keyCode == 40){
        moveBack = true;
    }

    /////////////  FOR CONTROLLER USE PAN AS IN TRACKBALL FOR LOCAL POS CHANGE

}
function keyUpHandle(event){
    if(event.keyCode == 37){
        moveLeft = false;
    }
    if(event.keyCode == 38){
        moveForward = false;
    }
    if(event.keyCode == 39){
        moveRight = false;
    }
    if(event.keyCode == 40){
        moveBack = false;
    }
    /////////////  FOR CONTROLLER USE PAN AS IN TRACKBALL FOR LOCAL POS CHANGE
}

setInterval(movePlayer, 25);
function movePlayer(){
    if(moveLeft) camera.position.x -= walkSpeed;  
    if(moveForward) camera.position.z -= walkSpeed;
    if(moveRight) camera.position.x += walkSpeed;
    if(moveBack) camera.position.z += walkSpeed;


}

function animate(){
    requestAnimationFrame(animate);
    cube.rotation.x += 0.01;
    cube.rotation.y += 0.02;
    cube.rotation.z += 0.01;
    mesh2.rotation.y += 0.03;
    render()
}

function onWindowResize(){
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
    render();
}

function render(){
    renderer.render(scene, camera);    
}

animate();








////////////////
// Voir comment repeat les textures via map: URL
// tout en utilisant material pas Basic

// Inclure WebGL dans un canvas