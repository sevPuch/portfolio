
let pictures = $("#pictures");
let imgFolder = "../img/";
console.log(pictures);

let anim = $(".anim");
createShape();

let slotsNb = 8;
assignSlots(slotsNb);

let modal = document.getElementById("myModal");
let modalImg = document.getElementById("img01");
let span = document.getElementsByClassName("close")[0];
console.log(span);
span.onclick = function() { 
    modal.style.display = "none";
}

function createShape(){

    for(let i = 0; i<6; i++){
        
        let shape = $("<div>");
        shape.addClass("shape");
        shape.prop("id", "cube"+i);

        let image = $("<img>");
        image.prop("src", "img/cube.png");
        shape.append(image);
        anim.append(shape);
        
    }
    for(let i = 0; i<6; i++){
        
        let shape = $("<div>");
        shape.addClass("shape");
        shape.prop("id", "tri"+i);

        let image = $("<img>");
        image.prop("src", "img/tri.png");
        shape.append(image);
        anim.append(shape);
        
    }
}

function assignSlots(slotsNb){
    for(let i = 0; i < slotsNb; i++){
        let slot = $("<div>");
        slot.addClass("slot");

        let link

        let img = $("<img>");
        let hasURL = false;

        $.get("img/"+i+".jpg", (data, status)=>{
            img.prop("src", "img/"+ i + ".jpg")
        })

        $.get("img/"+i+".png", (data, status)=>{
            img.prop("src", "img/"+ i + ".png")
        })

        img.css("width", 100+"%").css("height", 100+"%").css("object-fit", "cover");
        console.log(img.src);  
        img.on("click", displayModal);
        slot.append(img);

        pictures.append(slot);
    }
}

function displayModal(){
    modal.style.display = "block";
    modalImg.src = this.src;
}
