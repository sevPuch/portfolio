// let renderer = new THREE.WebGLRenderer();
// renderer.setSize(window.innerWidth, window.innerHeight);
// renderer.setClearColor(0x000000);

// let scene = new THREE.Scene();

// let cam = new THREE.PerspectiveCamera(80, window.innerWidth, window.innerHeight, 0.5, 10000);

// let lightAmb = new THREE.AmbientLight(0xffffff, 0.5);
// let lightPoint = new THREE.PointLight(0xffffff, 0.5);
// scene.add(lightAmb);
// scene.add(lightPoint);

// //MODEL
// let loader = new THREE.JSONLoader();
// loader.load("poop/poop.js", handle_load);

// function handle_load(geometry, materials){
//     let mesh = new THREE.Mesh(geometry, materials);
//     scene.add(mesh);
//     mesh.position.z = -10;
// }


// //RENDER LOOP
// render();

// function render(){
//     renderer.render(scene, cam);
//     requestAnimationFrame(render);
// }




var renderer,
scene,
camera,
myCanvas = document.getElementById('myCanvas');

//RENDERER
renderer = new THREE.WebGLRenderer({
canvas: myCanvas, 
antialias: true
});
renderer.setClearColor(0x000000);
renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);

//CAMERA
camera = new THREE.PerspectiveCamera(35, window.innerWidth / window.innerHeight, 0.1, 1000 );

//SCENE
scene = new THREE.Scene();

//LIGHTS
var light = new THREE.AmbientLight(0xffffff, 0.5);
scene.add(light);

var light2 = new THREE.PointLight(0xffffff, 0.5);
scene.add(light2);


var loader = new THREE.JSONLoader();
// loader.load('monkey.json', handle_load);

loader.load('monkey_animated3.json', handle_load);

var mesh;
var mixer;

function handle_load(geometry, materials) {

//BASIC MESH
// var material = new THREE.MultiMaterial(materials);
// mesh = new THREE.Mesh(geometry, material);
// scene.add(mesh);
// mesh.position.z = -10;

//ANIMATION MESH
var material = new THREE.MeshLambertMaterial({morphTargets: true});

mesh = new THREE.Mesh(geometry, material);
scene.add(mesh);
mesh.position.z = -10;


//MIXER
mixer = new THREE.AnimationMixer(mesh);

var clip = THREE.AnimationClip.CreateFromMorphTargetSequence('talk', geometry.morphTargets, 30);
mixer.clipAction(clip).setDuration(1).play();
}


//RENDER LOOP
render();

var delta = 0;
var prevTime = Date.now();

function render() {

delta += 0.1;

if (mesh) {

    mesh.rotation.y += 0.01;

    //animation mesh
    // mesh.morphTargetInfluences[ 0 ] = Math.sin(delta) * 20.0;
}

if (mixer) {
    var time = Date.now();
    mixer.update((time - prevTime) * 0.001);
    prevTime = time;
}

renderer.render(scene, camera);

requestAnimationFrame(render);
}
