let scene, camera, mesh, loader, renderer;
let player = new THREE.Object3D();
let walkSpeed = 0.4;
let url = "poop/poop.js";

let moveForward, moveLeft, moveRight, moveBack;
document.addEventListener("keydown", keyDownHandle, false);
document.addEventListener("keyup", keyUpHandle, false);

let SCREEN_WIDTH = window.innerWidth*0.8;
let SCREEN_HEIGHT = window.innerHeight*0.8;

let container = document.getElementById("container");

// RESIZE LISTENER NOT WORKING
window.addEventListener( 'resize', onWindowResize, false );

//////////////////////////////////////////
/////////// Initialize //////////////////
///////////////////////////////
// RENDERER
renderer = new THREE.WebGLRenderer({antialias: true});
renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
container.appendChild(renderer.domElement);
// CAMERA
player.add(camera);
camera = new THREE.PerspectiveCamera(50, SCREEN_WIDTH/SCREEN_HEIGHT, 0.5, 10000);
camera.position.set(0, 5, 10);
camera.rotation.x = -10 * Math.PI/180;

// SCENE
scene = new THREE.Scene();
scene.background = new THREE.Color(0xffffff);

//////////////////////////// WORLD ///////
// Cube
let geometry = new THREE.BoxGeometry(2,2.4,5);
let material = new THREE.MeshBasicMaterial({color: 0x209910, roughness: 0.5, metalness: 1.0});
let cube = new THREE.Mesh(geometry, material);
cube.position.set(0,20,-10);
scene.add(cube);
// Ground
let groundGeom = new THREE.PlaneBufferGeometry(1000, 1000);
let groundMat = new THREE.MeshPhongMaterial({
    color: 0xf3f3f3,
    shininess: 1,
    metalness:0.5,
    roughness:0.1
});
let ground = new THREE.Mesh(groundGeom, groundMat);
ground.position.set(0, 0, 0);
ground.rotation.x = - Math.PI/2;
scene.add(ground);

// Fog
scene.fog = new THREE.Fog(0xffffff, 5, 50);


// load model from JSON file
// POOP LOADER
loader = new THREE.JSONLoader();
loader.load(
    "poop/poop.js",
    function(geometry){
        let poopText = new THREE.ImageUtils.loadTexture("poop/austin.jpg")
        let poopMat = new THREE.MeshStandardMaterial({
            // map: poopText,
            color: 0x904020,
            roughness: 0.6,
            metalness: 0
            // map.wrapS: THREE.RepeatWrapping,
            // neheTexture.wrapT: THREE.RepeatWrapping, 
            // side: THREE.DoubleSide
        })

        mesh = new THREE.Mesh(geometry, poopMat);        
        mesh.position.set(0,0,0);

        scene.add(mesh);
    }
)


////////////////////////
/////////// LUMIERES
let pointLight = new THREE.PointLight(0x555555, 5, 50);
pointLight.position.set(-10,15,0);
scene.add(pointLight);
scene.add(new THREE.PointLightHelper(pointLight, 1));

renderer.render(scene, camera);


/////////////////////
////// FUNCTIONS
function keyDownHandle(event){
    if(event.keyCode == 37){
        moveLeft = true;
    }
    if(event.keyCode == 38){
        moveForward = true;
    }
    if(event.keyCode == 39){
        moveRight = true;
    }
    if(event.keyCode == 40){
        moveBack = true;
    }

    /////////////  FOR CONTROLLER USE PAN AS IN TRACKBALL FOR LOCAL POS CHANGE

}
function keyUpHandle(event){
    if(event.keyCode == 37){
        moveLeft = false;
    }
    if(event.keyCode == 38){
        moveForward = false;
    }
    if(event.keyCode == 39){
        moveRight = false;
    }
    if(event.keyCode == 40){
        moveBack = false;
    }
    /////////////  FOR CONTROLLER USE PAN AS IN TRACKBALL FOR LOCAL POS CHANGE
}

setInterval(movePlayer, 50);
function movePlayer(){
    if(moveLeft) camera.position.x -= walkSpeed;  
    if(moveForward) camera.position.z -= walkSpeed;
    if(moveRight) camera.position.x += walkSpeed;
    if(moveBack) camera.position.z += walkSpeed;


}

function animate(){
    requestAnimationFrame(animate);
    cube.rotation.x += 0.01;
    cube.rotation.y += 0.02;
    cube.rotation.z += 0.01;
    mesh.rotation.y += 0.03;
    render()
}

function onWindowResize(){
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
    render();
}

function render(){
    renderer.render(scene, camera);    
}

animate();








////////////////
// Voir comment repeat les textures via map: URL
// tout en utilisant material pas Basic

// Inclure WebGL dans un canvas