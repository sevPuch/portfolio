/**
 * @author alteredq / http://alteredqualia.com/
 * @author mr.doob / http://mrdoob.com/
 */

let Detector = {
    
        canvas: !! window.CanvasRenderingContext2D,
        webgl: ( function () {
    
            try {
    
                var canvas = document.createElement( 'canvas' ); return !! ( window.WebGLRenderingContext && ( canvas.getContext( 'webgl' ) || canvas.getContext( 'experimental-webgl' ) ) );
    
            } catch ( e ) {
    
                return false;
    
            }
    
        } )(),
        workers: !! window.Worker,
        fileapi: window.File && window.FileReader && window.FileList && window.Blob,
    
        getWebGLErrorMessage: function () {
    
            var element = document.createElement( 'div' );
            element.id = 'webgl-error-message';
            element.style.fontFamily = 'monospace';
            element.style.fontSize = '13px';
            element.style.fontWeight = 'normal';
            element.style.textAlign = 'center';
            element.style.background = '#fff';
            element.style.color = '#000';
            element.style.padding = '1.5em';
            element.style.width = '400px';
            element.style.margin = '5em auto 0';
    
            if ( ! this.webgl ) {
    
                element.innerHTML = window.WebGLRenderingContext ? [
                    'Your graphics card does not seem to support <a href="http://khronos.org/webgl/wiki/Getting_a_WebGL_Implementation" style="color:#000">WebGL</a>.<br />',
                    'Find out how to get it <a href="http://get.webgl.org/" style="color:#000">here</a>.'
                ].join( '\n' ) : [
                    'Your browser does not seem to support <a href="http://khronos.org/webgl/wiki/Getting_a_WebGL_Implementation" style="color:#000">WebGL</a>.<br/>',
                    'Find out how to get it <a href="http://get.webgl.org/" style="color:#000">here</a>.'
                ].join( '\n' );
    
            }
    
            return element;
    
        },
    
        addGetWebGLMessage: function ( parameters ) {
    
            var parent, id, element;
    
            parameters = parameters || {};
    
            parent = parameters.parent !== undefined ? parameters.parent : document.body;
            id = parameters.id !== undefined ? parameters.id : 'oldie';
    
            element = Detector.getWebGLErrorMessage();
            element.id = id;
    
            parent.appendChild( element );
    
        }
    
    };
    
    // browserify support
    if ( typeof module === 'object' ) {
    
        module.exports = Detector;
    
    }
/////////////////////////////////////////////////////
let scene = new THREE.Scene();
scene.background = new THREE.Color(0x01111f);
let camera = new THREE.PerspectiveCamera(120, window.innerWidth / window.innerHeight, 0.1, 1000);
camera.position.z = 7;
camera.lookAt(new THREE.Vector3(0,0,0));

let renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);
//// LIGHT//////////
let light = new THREE.PointLight(0xffffff);
light.position.set(0, 2, 0);
light.castShadow = true;
light.shadow.camera.zoom = 4;
scene.add(light);

////////////////////////////////////////////////////
/////// CUBE CREATION /////////////////////////////
//////////////////////////////////////////////////
let geometry = new THREE.BoxGeometry(2,2.4,5);
let material = new THREE.MeshBasicMaterial({color: 0x209910, roughness: 0.5, metalness: 1.0});
let cube = new THREE.Mesh(geometry, material);
scene.add(cube);
////////////////////////////////////////////////////
/////// CUBE 2 CREATION ///////////////////////////
//////////////////////////////////////////////////
let cubeGeom = new THREE.BoxGeometry(20,20,1);
let austinTexture = new THREE.TextureLoader().load("austin.jpg");
let austinMat = new THREE.MeshBasicMaterial({map: austinTexture});
let cube2 = new THREE.Mesh(cubeGeom, austinMat);
cube2.position.z = -5;
cube2.position.y = 5;
scene.add(cube2);

////////////////////////////////////////////////////
/////// LINE CREATION /////////////////////////////
//////////////////////////////////////////////////
let lineMat = new THREE.LineBasicMaterial({color: 0xaa00ff});
let lineGeom = new THREE.Geometry();
lineGeom.vertices.push(new THREE.Vector3(-10,0,0));
lineGeom.vertices.push(new THREE.Vector3(0,10,0));
lineGeom.vertices.push(new THREE.Vector3(10,0,0));
let line = new THREE.Line(lineGeom, lineMat);
scene.add(line);
renderer.render(scene, camera);

/////// ANIMATION /////////////////////////
let animate = function(){
    requestAnimationFrame(animate);
    renderer.render(scene, camera);
    cube.rotation.x += 0.01;
    cube.rotation.y += 0.02;
    cube.rotation.z += 0.015;

    cube2.rotation.z += 0.005;
}

animate();




//tester clearColor
// ouvrir dans un canvas dédié

// quand clic - curseur disparait, on peut bouger camera.
// clic laché, curseur réapparait, pas de contrôle sur camera.